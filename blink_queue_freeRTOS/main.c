#include "pico/stdlib.h"
#include "FreeRTOS.h"
#include <queue.h>
#include "task.h"
#include <stdio.h>
#include "hardware/adc.h"

#include "pico/cyw43_arch.h"

#include "hardware/vreg.h"      // scan wifi
#include "hardware/clocks.h"    // scan wifi

const uint LED_PIN = 16;
static QueueHandle_t xQueue = NULL;

void vBlinkTask()
{
float uIReceiveValue;  

    while(1)
    {
        gpio_put(LED_PIN, 1);
        vTaskDelay(5000);
        printf("*** parpadeo led *** \n");
        gpio_put(LED_PIN, 0);
        vTaskDelay(5000);

        xQueueReceive(xQueue,&uIReceiveValue,portMAX_DELAY);
        //printf(" %04X \n",uIReceiveValue);
        printf(" %.2f \n",uIReceiveValue);
    }
}

void adc_task()
{
    float uIValueToSend ;  // valor de queue
    float data=2.5;
    const float conv_factor = 3.3f / (1 << 12);             // factor de conversion fijo
    uint16_t result, cont=0;
      int aux=5;

    while(1)
    {
        result = adc_read();                // 12-bit conversion, assume max value == ADC_VREF == 3.3 V
        data = result * conv_factor;
        //temperatura = 27-(data - 0.706)/0.001721;
        // DEBUG_printf("temp: %2.1f ºC \n", temperatura);
        
        uIValueToSend = data;
        // Sending ON data to the queue
        aux= xQueueSend(xQueue,&uIValueToSend,portMAX_DELAY );
        if(aux != 0)
        {
            printf("cola vacia \n");
            cont++;
            
            printf("cont: %d seg \n", cont);
        }
        else if (aux == 0)
        { 
            vTaskDelay(1000); 
            printf("cola llena \n");
        }
        vTaskDelay(1); 
    }

}


void main()
{
   
    stdio_init_all(); 

    if (cyw43_arch_init())
    {
        printf("Wi-Fi init failed");
    }
    else{
            printf("Wi-Fi connected \n");
    }
    printf("a1");
    cyw43_arch_enable_sta_mode();           //para conexiones wifi

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
                 // para puerto serie 
    adc_init();
    adc_gpio_init(26);           // ADC0 
    adc_select_input(0);        // Select ADC input 0 (GPIO26) 


    xQueue = xQueueCreate(1,sizeof(float));
    if( xQueue != NULL )
    {
        printf("***--- queue creado ---*** \n");
    }

    printf("a2");

    xTaskCreate(vBlinkTask, "Blink Task", 256, NULL, 0, NULL);
      // Create Your ADC Task
    xTaskCreate(adc_task,"ADC_TASK",256,NULL,1,NULL);

    vTaskStartScheduler();

}
