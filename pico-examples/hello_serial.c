/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <FreeRTOS.h>
#include <queue.h>
#include <task.h>

#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"

#include "hardware/structs/rosc.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"

#include <string.h>
#include <time.h>

#include "lwip/pbuf.h"
#include "lwip/tcp.h"
#include "lwip/dns.h"

#include "lwip/altcp_tcp.h"
#include "lwip/altcp_tls.h"
#include "lwip/apps/mqtt.h"

#include "lwip/apps/mqtt_priv.h"

//********** mis defines *************
static QueueHandle_t xQueue = NULL;


#define LED_ROJO        16
#define BUTTON_GPIO_17  17

#define DEBUG_printf printf

#define MQTT_SERVER_HOST "telemetria.inti.gob.ar"
#define MQTT_SERVER_PORT 1883

#define PUBLISH_TOPIC  "inti/dma/pruebas/humedad"

#define WILL_TOPIC "inti/dma/pruebas/temperature"        /*uvyt: ultima voluntad y testamentp*/
#define WILL_MSG "sin conexion a broker"             /*uvyt: ultima voluntad y testamentp*/

#define MS_PUBLISH_PERIOD 5000
#define MQTT_TLS 0 // needs to be 1 for AWS IoT

typedef struct MQTT_CLIENT_T_ {
    ip_addr_t remote_addr;
    mqtt_client_t *mqtt_client;
    u8_t receiving;
    u32_t received;
    u32_t counter;
    u32_t reconnect;
} MQTT_CLIENT_T;

void led_task(void *pvParameters);
void adc_task(void *pvParameters);
void publish_task(void *pvParameters);
//void poll_task(void *pvParameters);



const uint LED_PIN = 16;
const uint DOOR_SWITCH_PIN = 17;
const uint MAX_TIMINGS = 85;

err_t mqtt_app_connect(MQTT_CLIENT_T *client);
void perform_payload( char *p);

// Perform initialisation
static MQTT_CLIENT_T* mqtt_client_init(void)
{ 
    MQTT_CLIENT_T *client = calloc(1, sizeof(MQTT_CLIENT_T));
    
    if (!client) {
        DEBUG_printf("failed to allocate state\n");
        return NULL;
    }
    client->receiving = 0;
    client->received = 0;
    return client;
}

void dns_found(const char *name, const ip_addr_t *ipaddr, void *callback_arg)
{
    MQTT_CLIENT_T *client = (MQTT_CLIENT_T*)callback_arg;
    DEBUG_printf("DNS query finished with resolved addr of %s.\n", ip4addr_ntoa(ipaddr));
    client->remote_addr = *ipaddr;
}

void run_dns_lookup(MQTT_CLIENT_T *client)
{
    DEBUG_printf("Running DNS query for %s.\n", MQTT_SERVER_HOST);

    cyw43_arch_lwip_begin();
    err_t err = dns_gethostbyname(MQTT_SERVER_HOST, &(client->remote_addr), dns_found, client);
    cyw43_arch_lwip_end();

    if (err == ERR_ARG) {
        DEBUG_printf("failed to start DNS query\n");
        return;
    }

    if (err == ERR_OK) {
        DEBUG_printf("no lookup needed");
        return;
    }

    while (client->remote_addr.addr == 0) {
        cyw43_arch_poll();
        sleep_ms(1);
    }
}

static void mqtt_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status)
{
    //MQTT_CLIENT_T *client = (MQTT_CLIENT_T *)arg;
    if (status != 0) {
        DEBUG_printf("Error during connection: err %d.\n", status);
    } else {
        DEBUG_printf("MQTT connected.\n");
    }
}

/* 
 * Called when publish is complete either with success or failure
 */
void mqtt_pub_request_cb(void *arg, err_t err) {
    MQTT_CLIENT_T *client = (MQTT_CLIENT_T *)arg;
    DEBUG_printf("msg num: %d \n",client->receiving);
    //client->receiving;
    //client->received++;
}

/*
 * The app publishing
 */
err_t mqtt_app_publish(MQTT_CLIENT_T *client, char *payload)
{
    //char payload[128];
    err_t err;
    u8_t qos = 2;       /* 0 1 or 2, see MQTT specification */
    u8_t retain = 0;

    // prepare payload to publish
    sprintf(payload, "hello from picow %d/%d", client->received, client->counter);
    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);      //prendo led al enviar dato
    cyw43_arch_lwip_begin();
    err = mqtt_publish(client->mqtt_client, PUBLISH_TOPIC , payload, strlen(payload), qos, retain, mqtt_pub_request_cb, client);
    cyw43_arch_lwip_end();
    //cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);       //apago led
    if(err != ERR_OK) {
        DEBUG_printf("**** Publish fail***** %d/%d\n", err, ERR_OK);
    }

    return err;
}

/* Initiate client and connect to server, if this fails immediately an error code is returned
 * otherwise mqtt_connection_cb will be called with connection result after attempting to
 * to establish a connection with the server. For now MQTT version 3.1.1 is always used
 */
err_t mqtt_app_connect(MQTT_CLIENT_T *client)
{
    struct mqtt_connect_client_info_t ci;
    err_t err;

    memset(&ci, 0, sizeof(ci));

    ci.client_id = "PicoW";
    ci.client_user = NULL;
    ci.client_pass = NULL;
    // ci.keep_alive = 0;
    // ci.will_topic = NULL;
    // ci.will_msg = NULL;
    // ci.will_retain = 0;
    // ci.will_qos = 0;
    ci.keep_alive = 60;
    ci.will_topic = WILL_TOPIC;
    ci.will_msg = WILL_MSG;
    ci.will_retain = 0;
    ci.will_qos = 2;

    err = mqtt_client_connect(client->mqtt_client, &(client->remote_addr), MQTT_SERVER_PORT, mqtt_connection_cb, client, &ci);
    //original err = mqtt_client_connect(state->mqtt_client, &(state->remote_addr), MQTT_SERVER_PORT, mqtt_connection_cb, state, &ci, mqtt_test_conn_config_cb);
    //err_t          mqtt_client_connect(mqtt_ent_t  client, nst ip_addr_t *ipaddr, u16_t port,       mqtt_connection_cb, *arg ,  ci);

    if (err != ERR_OK) {
        DEBUG_printf("mqtt_connect FAIL \n");
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
    }

    return err;
}

void app_run_in_loop(MQTT_CLIENT_T *client)
{
    client->mqtt_client = mqtt_client_new();
    client->counter = 0;

    err_t error;
    u32_t publish_period = MS_PUBLISH_PERIOD; // in ms

    char payload[128];

    if (client->mqtt_client == NULL) {
        DEBUG_printf("Failed to create new mqtt client\n");
        return;
    }

    if (mqtt_app_connect(client) == ERR_OK)
    {
        while (true)
        {
            cyw43_arch_poll();

            gpio_init(LED_ROJO);                //inicia  led rojo y button
            gpio_init(BUTTON_GPIO_17);

            gpio_set_dir(LED_ROJO,GPIO_OUT);        //config como entrada o salida
            gpio_set_dir(BUTTON_GPIO_17,GPIO_IN);

             if(gpio_get(BUTTON_GPIO_17) == 1)
             {
                DEBUG_printf(" button on \n");
                gpio_put(LED_ROJO,1);
             }

            if ( !publish_period-- )
            {
                if (mqtt_client_is_connected(client->mqtt_client))
                {
                    //cyw43_arch_lwip_begin();
                    client->receiving = 1;
                    perform_payload( payload );
                    error = mqtt_app_publish(client, payload);   // <-- Publish!

                    if ( error == ERR_OK) {
                        DEBUG_printf("publish numero: %d\n", client->counter);
                        client->counter++;
                    } // else ringbuffer is full and we need to wait for messages to flush.
                    
                    //cyw43_arch_lwip_end();
                }
                else {
                    DEBUG_printf(".");
                }

                publish_period = MS_PUBLISH_PERIOD;
            }
        }
    }
    else
        DEBUG_printf("Failed mqtt_test_connect()\n");

}

void perform_payload( char *p)
{
    time_t s;
    struct tm* current_time;

    char status[10];
       
    if( gpio_get(DOOR_SWITCH_PIN) ){
        gpio_put(LED_PIN, 1);
        strcpy(status,"ON");
    }
    else{
        gpio_put(LED_PIN, 0);
        strcpy(status,"OFF");
    }

    // time in seconds
    s = time(NULL);
     // to get current time
    current_time = localtime(&s);
 
    sprintf( p, "%02d:%02d:%02d-%s",
           current_time->tm_hour,
           current_time->tm_min,
           current_time->tm_sec,
           status
           );
}



int main()
{
   // char WIFI_SSID[] = "wifi01-ei";
   // char WIFI_PASSWORD[] = "Ax32MnF1975-ReB";    //contraseña valida
    stdio_init_all();
    adc_init();
    adc_gpio_init(26);           // Make sure GPIO26 **** ADC0 ****
    adc_select_input(0);        // Select ADC input 0 (GPIO26)
    // adc_set_temp_sensor_enabled(true);
    //adc_select_input(4); // imput 4 - sensor temperatura

    if (cyw43_arch_init())
    {
        printf("Wi-Fi init failed");
        return -1;
    }

    cyw43_arch_enable_sta_mode();

    //*****************************************************************
    DEBUG_printf("Connecting to WiFi...\n");
    if (cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASSWORD, CYW43_AUTH_WPA2_AES_PSK, 30000)) {
        DEBUG_printf("failed to  connect.\n");
        return 1;
    } else {
        DEBUG_printf("*** WIFI Connected.*** \n");
    }
    //*****************************************************************
    //*************** Create mqtt client ***************
    MQTT_CLIENT_T *client = mqtt_client_init();
    run_dns_lookup(client); 

    // Create your Queue
    xQueue = xQueueCreate(
        100,            // Length of the Queue (we set one because we only want one item in the Queue)
        sizeof(float)  // Size of the item(s) stored
    );

    if( xQueue != NULL )
    {
        /* Queue was not created and must not be used. */
        DEBUG_printf("***--- queue creado ---*** \n");
    }


     // Create Your publish Task
    xTaskCreate(
        publish_task,  // Task to be run
        "PUBLISH_TASK",      // Name of the Task for debugging and managing its Task Handle
        256,             // Stack depth to be allocated for use with task's stack (see docs)
        client,            // Arguments needed by the Task (NULL because we don't have any)
        1,               // Task Priority - Higher the number the more priority [max is (configMAX_PRIORITIES - 1) provided in FreeRTOSConfig.h]
        NULL             // Task Handle if available for managing the task
    );
     // Create Your ADC Task
    xTaskCreate(
        adc_task,  // Task to be run
        "ADC_TASK",      // Name of the Task for debugging and managing its Task Handle
        256,             // Stack depth to be allocated for use with task's stack (see docs)
        NULL,            // Arguments needed by the Task (NULL because we don't have any)
        1,               // Task Priority - Higher the number the more priority [max is (configMAX_PRIORITIES - 1) provided in FreeRTOSConfig.h]
        NULL             // Task Handle if available for managing the task
    );

     // Create Your LED Task
    xTaskCreate(
        led_task,  // Task to be run
        "LED_task",      // Name of the Task for debugging and managing its Task Handle
        256,             // Stack depth to be allocated for use with task's stack (see docs)
        NULL,            // Arguments needed by the Task (NULL because we don't have any)
        1,               // Task Priority - Higher the number the more priority [max is (configMAX_PRIORITIES - 1) provided in FreeRTOSConfig.h]
        NULL             // Task Handle if available for managing the task
    );

   

 
    client->mqtt_client = mqtt_client_new();
    client->counter = 0;

    if (client->mqtt_client == NULL)
    {
        DEBUG_printf("Failed to create new mqtt client\n");
        return 0;
    }

    
    DEBUG_printf("*** inicio scheduler*** \n");
    // loop forever
    //app_run_in_loop(my_client);
    // ------------
    //***************************************************
    vTaskStartScheduler();
    cyw43_arch_deinit();
    
    while (true) {
        // Your program should never get here
        DEBUG_printf("Failed TASK \n");
    };

    return 0;
   
}


//*******************************************************************
void led_task(void *pvParameters)
{
   
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);  //prendo led
        vTaskDelay(1000);  // Delay by TICKS defined by FreeRTOS priorities
        //cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
       // vTaskDelay(1000);

}
        
//*******************************************************************
void adc_task(void *pvParameters)
{
    uint uIValueToSend ;  // valor de queue
    float data;
    const float conv_factor = 3.3f / (1 << 12);             // factor de conversion fijo
    uint16_t result;

        result = adc_read();                // 12-bit conversion, assume max value == ADC_VREF == 3.3 V
        data = result * conv_factor;
        //temperatura = 27-(data - 0.706)/0.001721;
        // DEBUG_printf("temp: %2.1f ºC \n", temperatura);
        
        uIValueToSend = data;
        // Sending ON data to the queue
        xQueueSend(
            xQueue,          // The queue you created
            &uIValueToSend,  // Pointer for value to send to the queue
            100               // Delay for sending to queue (0 for now since our queue should be empty)
        );
        vTaskDelay(500); 
        //sleep_ms(500);

}
//*******************************************************************
void publish_task(void *pvParameters)
{
    //app_run_in_loop(my _client);
    uint uIReceiveValue;  // Value to retrieve from the Queue

    err_t error;
    u32_t publish_period = MS_PUBLISH_PERIOD; // in ms

    char payload[1];

    MQTT_CLIENT_T * client = (MQTT_CLIENT_T *) pvParameters;
    if (mqtt_app_connect(client) == ERR_OK)
    {
        
        DEBUG_printf("create new mqtt client\n %p",pvParameters );
        while(true)
            {
                cyw43_arch_poll();   
                vTaskDelay(1);
                DEBUG_printf("cyarch bien \n");

                // Receiving LED data from the queue
                xQueueReceive(
                xQueue,           // The queue you created
                &uIReceiveValue,  // Pointer for value to store the received data from the queue
                portMAX_DELAY     // Delay for sending to queue (we set max delay to wait for the value)
                );

            sprintf(payload,"%1.2f",uIReceiveValue);
            if ( !publish_period-- )
            {
                if (mqtt_client_is_connected(client->mqtt_client) )
                {
                    printf("Dato recibido\n");
                    DEBUG_printf("voltage: %f V\n", payload);
                    //cyw43_arch_lwip_begin();
                    client->receiving = 1;
                    perform_payload(payload);
                    error = mqtt_app_publish(client, payload);   // <-- Publish!

                    if ( error == ERR_OK) {
                        DEBUG_printf("publish numero: %d\n", client->counter);
                        client->counter++;
                    } // else ringbuffer is full and we need to wait for messages to flush.
                    
                    //cyw43_arch_lwip_end();
                }
                else {
                    DEBUG_printf(".");
                }

                publish_period = MS_PUBLISH_PERIOD;
            }

        }
    }
    else
            DEBUG_printf("Failed mqtt_test_connect()\n");
}
